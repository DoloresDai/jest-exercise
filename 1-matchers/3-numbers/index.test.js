import { sum } from '../1-basic/index';

beforeEach(() => {
  expect.hasAssertions();
});

test('demo', () => {
  const result = sum(1, 1);
  const value = 1;

  expect(result).toBeGreaterThan(value);
});

test('result比3小', () => {
  const result = sum(1, 1);
  const value = 3;

  // <--start
  // TODO: 给出正确的assertion
  expect(result).toBeLessThan(value);
  // --end->
});
test('result等于3.0', () => {
  const result = sum(0.1, 0.2);
  const value = 0.3;

  // <--start
  // TODO: 给出正确的assertion
  expect(result).toBeCloseTo(value);
  // --end->
});
